## drupal/inspect ##

### Inspect is a service ###

In production environment it's highly recommended to stick to the service.
```PHP
/** @var \Drupal\inspect\Inspect $inspect */
$inspect = \Drupal::service('inspect.inspect');
/** @var \Drupal\inspect\Inspector $inspector */
$inspector = $inspect->variable($subject);

\Drupal::service('inspect.inspect')->variable($subject)->log();
\Drupal::service('inspect.inspect')->trace($throwableOrNull)->log();
```

### Principal methods and options ###

See [simplecomplex/inspect's](https://github.com/simplecomplex/inspect/blob/master/README.md) readme.

drupal/inspect supports an extra (log) 'channel' option for the inspector's chainable log() method:
```PHP
->log($level = 'debug', $message = '', array $context = ['channel' => 'inspect'])
```
