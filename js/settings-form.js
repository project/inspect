(($, Drupal) => {
  // Configuration pattern behavior.
  Drupal.behaviors.inspectSettingsForm = {
    // Set config vars when selecting 'database' or 'file' pattern.
    attach: (context) => {
      const outputMax = {
        database: 65536,
        file: 8192,
      };
      const $outputMax = $('input[name="inspect_output_max"]', context);
      const $escapeHtml = $('input[name="inspect_escape_html"]', context);

      $('input[name="config_pattern"][value="database"]')
        .not('[settings-form-once]')
        .attr('settings-form-once', '1')
        .click(() => {
          $outputMax.val(outputMax.database);
          $escapeHtml.get(0).checked = true;
        });

      $('input[name="config_pattern"][value="file"]')
        .not('[settings-form-once]')
        .attr('settings-form-once', '1')
        .click(() => {
          $outputMax.val(outputMax.file);
          $escapeHtml.get(0).checked = false;
        });
    },
  };
})(jQuery, Drupal);
