<?php

namespace Drupal\inspect\Helper;

use Drupal\Component\Utility\Unicode as UnicodeUtilDrupal;
use SimpleComplex\Inspect\Helper\Unicode as UnicodeBase;

/**
 * Unicode string helper.
 */
class Unicode extends UnicodeBase {

  /**
   * {@inheritDoc}
   */
  public function strlen($var): int {
    $v = '' . $var;
    if ($v === '') {
      return 0;
    }
    return mb_strlen($v);
  }

  /**
   * {@inheritDoc}
   */
  public function strpos(mixed $haystack, mixed $needle): bool|int {
    $hstck = '' . $haystack;
    $ndl = '' . $needle;
    if ($hstck === '' || $ndl === '') {
      return FALSE;
    }
    return mb_strpos($hstck, $ndl);
  }

  /**
   * {@inheritDoc}
   */
  public function substr($var, int $start, ?int $length = NULL): string {
    if ($start < 0) {
      throw new \InvalidArgumentException('Arg start is not non-negative integer.');
    }
    if ($length !== NULL && (!is_int($length) || $length < 0)) {
      throw new \InvalidArgumentException('Arg length is not non-negative integer or null.');
    }
    $v = '' . $var;
    if (!$length || $v === '') {
      return '';
    }
    return mb_substr($v, $start, $length);
  }

  /**
   * {@inheritDoc}
   */
  public function truncateToByteLength(mixed $var, int $length): string {
    if ($length < 0) {
      throw new \InvalidArgumentException('Arg length is not non-negative integer.');
    }

    $v = '' . $var;
    return UnicodeUtilDrupal::truncateBytes($v, $length);
  }

}
