<?php

namespace Drupal\inspect\Helper;

use SimpleComplex\Inspect\Helper\Config as ConfigBase;

/**
 * Configuration helper, bridging Drupal module settings with base Inspect.
 */
class Config extends ConfigBase {

  /**
   * Get a configuration property.
   *
   * @param string $name
   *   Property name.
   *
   * @return mixed|null
   *   The config var value. Null if non-existent or has null value.
   *
   * @throws \OutOfBoundsException
   *   If unsupported configuration property name.
   */
  public function __get(string $name) {
    if (array_key_exists($name, static::PROPERTIES)) {
      return $this->config->get($name) ?? NULL;
    }
    throw new \OutOfBoundsException(
      'Inspect configuration ' . get_class($this) . ' instance exposes no property[' . $name . '].'
    );
  }

}
