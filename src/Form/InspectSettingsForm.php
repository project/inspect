<?php

namespace Drupal\inspect\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inspect\Inspector;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure inspect settings for this site.
 */
class InspectSettingsForm extends ConfigFormBase {

  /**
   * Config var type by name.
   */
  const INSPECT_CONFIG_VARS = [
    'depth' => 'integer',
    'trace_depth' => 'integer',
    'trace_limit' => 'integer',
    'truncate' => 'integer',
    'escape_html' => 'boolean',
    'output_max' => 'integer',
    'exectime_percent' => 'integer',
    'rootdir_replace' => 'boolean',
  ];

  /**
   * Recommended output max. length for various contexts.
   */
  const OUTPUT_MAX_PATTERNS = [
    'database' => 65536,
    'file' => 8192,
  ];

  /**
   * Core module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Overrides parent to get module handler injected.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /*
     * create() method implemented to get extra constructor injection(s);
     * in this case the module handler.
     * @see https://www.drupal.org/docs/8/api/services-and-dependency-injection/dependency-injection-for-a-form
     */
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'inspect_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'inspect.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('inspect.settings');

    if ($this->moduleHandler->moduleExists('help')) {
      $form['help'] = [
        '#type' => 'markup',
        '#markup' => '<p>Inspect\'s '
        . $this->t(
          '<a href="@href-inspect-help">help page</a>.',
          ['@href-inspect-help' => '/admin/help/inspect']
        )
        . '</p>',
      ];
    }

    $form['log_target_pattern'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configure for log target and viewer'),
      'config_pattern' => [
        '#type' => 'radios',
        '#title' => $this->t('Pattern'),
        '#options' => [
          'custom' => $this->t('Custom'),
          'database' => $this->t('Database + dblog viewer'),
          'file' => $this->t('File + Grafana'),
        ],
        '#default_value' => 'custom',
        '#attributes' => [
          'autocomplete' => 'off',
        ],
      ],
    ];
    $form['#attached']['library'][] = 'inspect/settings-form';
    $output_max = $config->get('output_max');
    $escape_html = $config->get('escape_html');
    if ($output_max == static::OUTPUT_MAX_PATTERNS['database'] && $escape_html) {
      $form['log_target_pattern']['config_pattern']['#default_value'] = 'database';
    }
    elseif ($output_max == static::OUTPUT_MAX_PATTERNS['file'] && !$escape_html) {
      $form['log_target_pattern']['config_pattern']['#default_value'] = 'file';
    }

    $form['output_fuses'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fuses - graceful degradation'),
      '#description' => $this->t(
        'Maximum output lengths of dumps and traces secure that Inspect degrades gracefully when analyzing an overly big data set.
<br/>Inspections and traces also abort (cleanly) if more than a certain percent of PHP\'s max execution time has passed.'
      ),
      'inspect_output_max' => [
        '#type' => 'textfield',
        '#title' => $this->t('Maximum output length'),
        '#description' => $this->t(
          'Absolute max: @max. Default: @default.',
          [
            '@max' => Inspector::OUTPUT_MAX,
            '@default' => Inspector::OUTPUT_DEFAULT,
          ],
        ),
        '#size' => 8,
        '#required' => FALSE,
      ],
      'inspect_exectime_percent' => [
        '#type' => 'textfield',
        '#title' => $this->t(
          'Abort variable/trace inspection if called later than this percent of max execution time (@max_execution_time seconds)',
          ['@max_execution_time' => ini_get('max_execution_time')]
        ),
        '#description' => $this->t(
          "If PHP max_execution_time is being changed during run-time - set_time_limit() - this 'fuse' will not work predictably.
<br/>Because max_execution_time is only being checked at first inspection/trace in a request,
<br/>and any later changes will not be reflected by Inspect's time limit 'fuse' in that request."
        ),
        '#size' => 2,
        '#required' => FALSE,
      ],
    ];

    $form['inspection_tracing'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Inspection and tracing'),
      'inspect_depth' => [
        '#type' => 'textfield',
        '#title' => $this->t('Recursion depth when inspecting'),
        '#description' => $this->t(
          'Default value: @default.',
          ['@default' => Inspector::DEPTH_DEFAULT]
        ),
        '#size' => 2,
        '#required' => FALSE,
      ],
      'inspect_trace_depth' => [
        '#type' => 'textfield',
        '#title' => $this->t('Recursion depth of method/function arguments when tracing'),
        '#description' => $this->t(
          "Default value: @default. Minimum: 0 (~ don't inspect arguments).",
          ['@default' => Inspector::TRACE_DEPTH_DEFAULT]
        ),
        '#size' => 2,
        '#required' => FALSE,
      ],
      'inspect_trace_limit' => [
        '#type' => 'textfield',
        '#title' => $this->t('Stack frame limit when tracing'),
        '#description' => $this->t(
          'Recommended value: @default; a higher a value (deeper stack tracing) will rarely provide additional usable information.',
          ['@default' => Inspector::TRACE_LIMIT_DEFAULT]
        ),
        '#size' => 3,
        '#required' => FALSE,
      ],
      'inspect_truncate' => [
        '#type' => 'textfield',
        '#title' => $this->t('String truncation'),
        '#description' => $this->t(
          'Maximum: @max. Default: @default.',
          [
            '@max' => Inspector::TRUNCATE_MAX,
            '@default' => Inspector::TRUNCATE_DEFAULT,
          ],
        ),
        '#size' => 5,
        '#required' => FALSE,
      ],
      'inspect_escape_html' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Escape HTML'),
        '#description' => $this->t('Needed when using dblog viewer.'),
        '#attributes' => [
          'autocomplete' => 'off',
        ],
      ],
      'inspect_rootdir_replace' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Replace Drupal root dir from paths and strings'),
        '#attributes' => [
          'autocomplete' => 'off',
        ],
      ],
    ];

    // Set form var defaults, if previously saved value.
    foreach (static::INSPECT_CONFIG_VARS as $name => $type) {
      $existing = $config->get($name);
      if ($existing !== NULL) {
        if ($name == 'output_max' || $name == 'exectime_percent') {
          $form_definition =& $form['output_fuses'];
        }
        else {
          $form_definition =& $form['inspection_tracing'];
        }
        $form_definition['inspect_' . $name]['#default_value'] = match ($type) {
          'boolean' => (int) $existing,
          default => $existing,
        };
        unset($form_definition);
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    foreach (static::INSPECT_CONFIG_VARS as $name => $type) {
      $value = $form_state->getValue('inspect_' . $name);
      switch ($type) {
        case 'integer':
          if (!$value && ($value === 0 || $value === '0')) {
            // Only trace_depth is allowed to be zero.
            if ($name != 'trace_depth') {
              $form_state->setErrorByName(
                'inspect_' . $name,
                $this->t("'@name' must be positive.", ['@name' => $name])
              );
            }
          }
          // Empty string means don't set a value at all.
          elseif ($value !== '') {
            if (!ctype_digit('' . $value) || $value < 0) {
              $form_state->setErrorByName(
                'inspect_' . $name,
                $this->t("'@name' must be a positive integer.", ['@name' => $name])
              );
            }
            else {
              $max = match ($name) {
                'depth', 'trace_depth' => Inspector::DEPTH_MAX,
                'trace_limit' => Inspector::TRACE_LIMIT_MAX,
                'truncate' => Inspector::TRUNCATE_MAX,
                'output_max' => Inspector::OUTPUT_MAX,
                'exectime_percent' => Inspector::EXEC_TIMEOUT_MAX,
                default => -1,
              };
              if ($value > $max) {
                $form_state->setErrorByName(
                  'inspect_' . $name,
                  $this->t("'@name' cannot be more than @max.",
                    ['@name' => $name, '@max' => $max])
                );
              }
            }
          }
          break;

        default:
          // Cannot validate boolean.
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('inspect.settings');

    foreach (static::INSPECT_CONFIG_VARS as $name => $type) {
      $value = $form_state->getValue('inspect_' . $name);
      switch ($type) {
        case 'boolean':
          $config->set($name, (bool) $value);
          break;

        case 'integer':
          // Only trace_depth is allowed to be zero.
          if ($value || $name == 'trace_depth') {
            $config->set($name, (int) $value);
          }
          break;

        default:
          if ($value) {
            $config->set($name, $value);
          }
      }
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
