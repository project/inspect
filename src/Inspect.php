<?php

namespace Drupal\inspect;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\inspect\Helper\Config;
use Drupal\inspect\Helper\Unicode;
use SimpleComplex\Inspect\Inspect as InspectBase;

/**
 * Variable analyzer and exception tracer.
 *
 * Proxy class for Inspector.
 */
class Inspect extends InspectBase {

  const CLASS_CONFIG = Config::class;

  const CLASS_UNICODE = Unicode::class;

  const CLASS_INSPECTOR = Inspector::class;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   Core logger factory.
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * Create inspect instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Core configuration factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Core logger factory.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
  ) {
    parent::__construct();

    $this->getConfig(
      $config_factory->get('inspect.settings')
    );

    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritDoc}
   *
   * Real Drupal app root, like Symfony project directory; that is without
   * trailing /web.
   *
   * @see \Drupal::root()
   *   Drupal root.
   * @see https://www.drupal.org/project/drupal/issues/1672986
   *   Option to have all php files outside of web root.
   * @see https://www.drupal.org/project/drupal/issues/2385395
   *   Make Drupal core folder agnostic
   *   and allow it to be placed in vendor/drupal/core
   */
  public function rootDir(): string {
    if (!$this->rootDirLength) {
      // phpcs:ignore
      $this->rootDir = \Drupal::root();
      if (mb_stripos($this->rootDir, DIRECTORY_SEPARATOR . 'web', -4) !== FALSE
        && file_exists($this->rootDir . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'composer.json')
      ) {
        $this->rootDir = mb_substr($this->rootDir, 0, -4);
      }
      $this->rootDirLength = mb_strlen($this->rootDir);
      if (!$this->rootDirLength) {
        // Flag that root dir cannot be established.
        $this->rootDir = '';
        $this->rootDirLength = -1;
      }
    }
    return $this->rootDir;
  }

  /**
   * For the Inspector.
   *
   * Simpler than overriding Inspector constructor and every single Inspector
   * inspection/trace method.
   *
   * The logger channel concept is challenging in the sense that you cannot
   * simply inject a PSR logger.
   *
   * @return \Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   Core logger factory.
   *
   * @see Inspector::log()
   *   Using this method.
   */
  public function getLoggerFactory(): LoggerChannelFactoryInterface {
    return $this->loggerFactory;
  }

}
