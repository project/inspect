<?php

namespace Drupal\inspect;

use SimpleComplex\Inspect\Inspector as InspectorBase;

/**
 * Produces variable inspection or backtrace, stringable and loggable.
 */
class Inspector extends InspectorBase {

  /**
   * Default maximum byte (ASCII) length of an inspection/trace output.
   *
   * When using dblog and localization the recommended value is <=65536,
   * because in dblog event viewer the log entry will be translated (sic!)
   * and then max length of db (blob) locales_source.source effectively rules.
   *
   * @see https://www.drupal.org/project/drupal/issues/2843563
   *
   * @var int
   */
  const OUTPUT_DEFAULT = 65536;

  /**
   * {@inheritDoc}
   */
  const LIB_FILENAMES = [
    'inspect.module' => NULL,
  ] + InspectorBase::LIB_FILENAMES;

  /**
   * Convenience method allowing method chaining.
   *
   * E.g. \Drupal::service('inspect.inspect')->variable(...)->log().
   *
   * Works like and uses PSR logger->log().
   *
   * If $message is non-empty it gets prepended to inspection output.
   *
   * @param string|int $level
   *   Log level.
   * @param mixed $message
   *   Non-string becomes stringified.
   *   Non-empty gets prepended to inspection output.
   * @param mixed[] $context
   *   Array of context tokens.
   *
   * @return void
   *   None.
   *
   * @see \Drupal::logger()
   *   Drupal logger.
   * @see \Psr\Log\LoggerInterface::log()
   *   PSR logger interface.
   * @see \Psr\Log\LogLevel
   *   PSR log level.
   * @see \Drupal\Core\Logger\RfcLogLevel
   *   Drupal log level.
   */
  public function log(string|int $level = 'debug', mixed $message = '', array $context = ['channel' => 'inspect']): void {
    if (!empty($context['channel'])) {
      $channel = $context['channel'];
      unset($context['channel']);
    }
    else {
      $channel = 'inspect';
    }

    if (method_exists($this->proxy, 'getLoggerFactory')) {
      $this->proxy->getLoggerFactory()->get($channel)->log(
        $level,
        '' . $message . (!$message ? '' : "\n") . $this->__toString(),
        $context
      );
    }
    else {
      parent::log($level, $message, $context);
    }
  }

}
