# Changelog

All notable changes to **drupal/inspect** will be documented in this file,
using the [Keep a CHANGELOG](https://keepachangelog.com/) principles.

## [Unreleased]

### Added

### Changed

### Fixed

## [5.0.1] - 2024-12-04

### Changed
- Issue #3451982: Drupal 11 support.

### Fixed
- Code style (Multi-line function declarations must have a trailing comma after
  the last parameter).
- Code style (changelog).

## [5.0.0] - 2023-11-04

### Added
- Settings form now makes it easy to configure for database+dblog viewer
  and file+Grafana, respectively.

### Changed
- Compatible with PHP 8.1 and Drupal core ^10.x.
- Default settings are now optimized for file logging (and Grafana viewing).
- All deprecated methods and functions removed.
- Test module removed.

### Fixed
- Code style.

## [4.1.1] - 2021-04-19

### Changed
- Require simplecomplex/inspect ^3.1.1, force dependant to update.

## [4.1.0] - 2021-04-19

### Changed
- Settings `escape_html` default to true, for dblog viewer.

## [4.0.1] - 2020-08-18

### Changed
- Output max default reduced to match MySQL blob length (65535), to prevent
  dblog viewer failure when translating log event.
- Truncation default reduced to 100.

## [4.0.0] - 2020-08-11

### Changed
- All inspect permissions removed. Do only log inspections.
- Getting inspection is no longer supported, at least not in previous manner.
- Filing inspection is no longer supported.
- Logging from frontend is no longer supported.
- Session and page load counting features removed.
- inspect_test sub module uses drush or drupal console to log a few inspections.
- Attach CSS to dblog event page only.
- Attach no Javascript; the library hasn't been tested for 5 years.
- Changelog in standard keepachangelog format; previous was idiosyncratic.
- Updated lib SimpleComplex Inspect to 3.1.

### Fixed
- Depend on simplecomplex/inspect library via composer; instead of embedding.
- There shan't be any 2.x branch; ancient 8.x-2.4 release was a mistake.

## [8.x-1.0] - 2016-05-01

### Changed
- Updated SimpleComplex Inspect lib to 1.1.

### Fixed
- Fixed fatal log error due to SafeMarkup::checkPlain() not returning string,
by replacing with Html::checkPlain(); SafeMarkup is deprecated, anyway.
And our plaintext() method now removes tags; like simplecomplex' ditto does.
- Exclamation mark (pass-thru) format placeholder not supported in D8.
- Replaced calls to deprecated Drupal::url() with Url::fromRoute()->toString().
- Deprecated db_query() replaced by Drupal::database()->query().
- Don't set cookie in CLI mode.

## [8.x-1.0-beta1] - 2015-07-12

### Added
- Port from 7.x-6.3.
- New instance var code, usable for event or error code.
- Now supports logging via injected PSR-3 logger; instance var (object) logger.
- Added inspect_test module.

### Changed
- Backend and frontend: category is now an alias of type (instead of vice versa)
and the use of category is deprecated.

### Fixed
- (SimpleComplex Inspect) Frontend: stricter typeOf() array check; some jQuery
  extensions (old dataTables) were erroneously assessed as array.
- (SimpleComplex Inspect) Fixed frontend file:line resolver; failed to identify
  inspect.js self.
- Fixed that core String class is now renamed to SafeMarkup.
